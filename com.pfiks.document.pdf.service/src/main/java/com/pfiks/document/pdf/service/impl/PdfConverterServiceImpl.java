/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.document.pdf.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.itextpdf.html2pdf.ConverterProperties;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.document.pdf.exception.PdfConverterException;
import com.pfiks.document.pdf.service.PdfConverterService;

@Component(immediate = true, service = PdfConverterService.class)
public class PdfConverterServiceImpl implements PdfConverterService {

	private static final String PDF_EXTENSION = ".pdf";

	@Reference
	private PdfGenerationService pdfGenerationService;

	@Override
	public File generatePdfFromHtml(String html, String pdfFileName) throws PdfConverterException {

		validateContentAndFileNameParameters(html, pdfFileName);

		return pdfGenerationService.generatePdf(html, pdfFileName, new ConverterProperties());
	}

	@Override
	public File generatePdfFromHtml(String html, String pdfFileName, String resourcesBaseUri) throws PdfConverterException {

		validateParameters(html, pdfFileName, resourcesBaseUri);

		ConverterProperties converterProperties = new ConverterProperties();
		converterProperties.setBaseUri(resourcesBaseUri);

		return pdfGenerationService.generatePdf(html, pdfFileName, converterProperties);

	}

	@Override
	public byte[] generatePdfFromKeyValueMap(Map<String, Object> formFieldValueMap) throws PdfConverterException {
		return generatePdf(null, formFieldValueMap);
	}

	@Override
	public byte[] generatePdfFromKeyValueMap(String title, Map<String, Object> formFieldValueMap) throws PdfConverterException {
		return generatePdf(title, formFieldValueMap);
	}

	@Override
	public byte[] generatePdfFromKeyValuePairs(String title, List<KeyValuePair> keyValuePairs) throws PdfConverterException {
		return pdfGenerationService.generatePdf(title, keyValuePairs);
	}

	@Override
	public String readPdfContent(InputStream inputStream) throws IOException {
		try (PDDocument document = PDDocument.load(inputStream)) {
			PDFTextStripper pdfStripper = new PDFTextStripper();
			return pdfStripper.getText(document);
		}
	}

	private byte[] generatePdf(String title, Map<String, Object> formFieldValueMap) throws PdfConverterException {

		List<KeyValuePair> keyValuesPairs = new LinkedList<>();
		if (!MapUtil.isEmpty(formFieldValueMap)) {
			formFieldValueMap.keySet().stream().forEach(key -> {
				keyValuesPairs.add(new KeyValuePair(key, String.valueOf(formFieldValueMap.get(key))));
			});
		}

		return pdfGenerationService.generatePdf(title, keyValuesPairs);

	}

	private void validateContentAndFileNameParameters(String html, String pdfFileName) {

		if (Validator.isNull(pdfFileName) || !pdfFileName.endsWith(PDF_EXTENSION) || Validator.isNull(html)) {

			throw new IllegalArgumentException("Illegal arguments - the supplied parameters must not be null or empty and the file name must end with .pdf");

		}

	}

	private void validateParameters(String html, String pdfFileName, String resourcesBaseUri) {

		if (!Validator.isUri(resourcesBaseUri)) {
			throw new IllegalArgumentException("Illegal arguments - the supplied resources base uri must be a valid URI");
		}
		validateContentAndFileNameParameters(html, pdfFileName);
	}

}
