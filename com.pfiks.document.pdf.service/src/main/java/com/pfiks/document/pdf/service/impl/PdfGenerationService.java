/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.document.pdf.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.osgi.service.component.annotations.Component;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.SystemProperties;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.document.pdf.exception.PdfConverterException;

@Component(immediate = true, service = PdfGenerationService.class)
public class PdfGenerationService {

	public File generatePdf(String html, String pdfFileName, ConverterProperties converterProperties) throws PdfConverterException {

		File pdfFile = new File(SystemProperties.get(SystemProperties.TMP_DIR).concat(StringPool.SLASH).concat(pdfFileName));
		File htmlTempFile = FileUtil.createTempFile("html");

		try (PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile), new WriterProperties().setFullCompressionMode(true))) {

			FileUtil.write(htmlTempFile, html);
			HtmlConverter.convertToPdf(new FileInputStream(htmlTempFile), writer, converterProperties);

		} catch (IOException e) {

			throw new PdfConverterException("Error generating PDF from HTML", e);

		} finally {

			FileUtil.delete(htmlTempFile);

		}

		return pdfFile;
	}

	public byte[] generatePdf(String title, List<KeyValuePair> keyValuePairs) throws PdfConverterException {

		try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {

			Document document = new Document();
			com.itextpdf.text.pdf.PdfWriter.getInstance(document, byteArrayOutputStream);
			document.open();

			if (title != null) {
				addMainTitle(title, document);
			}
			addKeyValueAsParagraph(keyValuePairs, document);

			document.close();

			return byteArrayOutputStream.toByteArray();
		} catch (Exception e) {
			throw new PdfConverterException("Error generating PDF byte from formFieldValueMap", e);
		}

	}

	private void addKeyValueAsParagraph(List<KeyValuePair> keyValuePairs, Document document) throws DocumentException {

		Font fontQuestion = FontFactory.getFont(FontFactory.TIMES, 12.3f, Font.BOLD);
		Font fontAnswer = FontFactory.getFont(FontFactory.TIMES, 10.7f, Font.NORMAL);

		for (KeyValuePair keyValuePair : keyValuePairs) {

			String value = getStringCleanValue(keyValuePair.getValue());
			if (!value.isEmpty()) {
				Paragraph paragraphQuestion = new Paragraph(new Phrase(keyValuePair.getKey(), fontQuestion));
				paragraphQuestion.setSpacingBefore(5);
				document.add(paragraphQuestion);

				Paragraph paragraphAnswer = new Paragraph(new Phrase(value, fontAnswer));
				document.add(paragraphAnswer);
			}

		}
	}

	private void addMainTitle(String title, Document document) throws DocumentException {

		if (Validator.isNotNull(title)) {
			Paragraph paragraph = new Paragraph();
			paragraph.setFont(FontFactory.getFont(FontFactory.TIMES, 21.7f, Font.BOLD));
			paragraph.add(title);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.setSpacingAfter(10);
			document.add(paragraph);
		}

	}

	private String getStringCleanValue(String value) {
		if (Validator.isNotNull(value)) {
			return StringEscapeUtils.unescapeHtml4(value).replace("[]", StringPool.BLANK).replace("[\"", StringPool.BLANK).replace("\"]", StringPool.BLANK);
		}
		return StringPool.BLANK;
	}

}
