# com-pfiks-document

This project contains Liferay compatible OSGI modules that provide services for operations on document formats.
Services for common tasks such as conversions between document formats should be added to this repo.

## Example modules
* com-pfiks-document-excel
* com-pfiks-document-pdf
* com-pfiks-document-word

## Supported versions
* Liferay 7.4 update 92
* JDK 11