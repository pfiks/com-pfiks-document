#!groovy

def sonarArgs
boolean snapshotBuild

pipeline {

	agent {
		kubernetes {
			yamlFile "kubernetes/jenkins/pod.yaml"
		}
	}

	environment {
		SONAR_TOKEN = credentials('sonarcloud-token')
	}

	stages {

		stage('Initialise environment variables') {

			steps {

				script {

					if (env.BRANCH_NAME.startsWith('master/')) {

						sonarArgs = '-Dsonar.branch.name=' + env.BRANCH_NAME
						env.GRADLE_CACHE_PUSH_ENABLED = true

					} else {

						env.GRADLE_CACHE_PUSH_ENABLED = false

						sonarArgs = '-Dsonar.pullrequest.key=' + env.BRANCH_NAME.replaceAll('PR-','')
						sonarArgs += ' -Dsonar.pullrequest.branch=' + env.BRANCH_NAME

					}

					snapshotBuild = sh(script: 'git log -1 --pretty=%B | fgrep -ie "Set snapshot versions" -e "Set snapshot versions"', returnStatus: true) == 0

				}

			}

		}

		stage("Build") {

			when {
				expression { !snapshotBuild }
			}

			steps {

				sh './gradlew build javadoc'

				junit allowEmptyResults: true, testResults: '**/build/test-results/*.xml'

			}

		}

		stage("Code quality analysis") {

			when {
				expression { !snapshotBuild }
			}

			steps {

				sh './gradlew jacocoTestReport'
				sh './gradlew --stop'

				container("zulu-openjdk-17") {
				    sh "./gradlew sonar ${ sonarArgs } --no-daemon"
				    sh 'chmod -R ugo+rw $WORKSPACE'
				}

			}

		}

		stage("Publish") {

			when {
				allOf {
					branch pattern: "master/*"
					expression { !snapshotBuild }
				}
			}

			steps {

				sh './gradlew artifactoryPublish'

			}

		}

		stage("Prepare & commit snapshots") {

			when {
				allOf {
					branch pattern: "master/*"
					expression { !snapshotBuild }
				}
			}

			steps {

				sshagent (credentials: ['jenkins_ssh'], ignoreMissing: true) {

					sh 'ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts'
					sh './gradlew prepareAndCommitSnapshots'

				}

			}

		}

	}

	post {

		fixed {

			script {

				if (env.BRANCH_NAME.startsWith('master/')) {

					slackSend channel: '#build-issues',
							color: "good",
							message: "*${currentBuild.currentResult}:* ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"

				}

			}

		}

		regression {

			script {

				if (env.BRANCH_NAME.startsWith('master/')) {

					slackSend channel: '#build-issues',
							color: "danger",
							message: "*${currentBuild.currentResult}:* ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"

				}

			}

		}

	}

}
