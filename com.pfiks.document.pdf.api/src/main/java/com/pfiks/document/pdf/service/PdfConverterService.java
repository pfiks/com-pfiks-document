/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.document.pdf.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.util.KeyValuePair;
import com.pfiks.document.pdf.exception.PdfConverterException;

/**
 * The PDF converter service interface.
 */
public interface PdfConverterService {

	/**
	 * Generate a PDF from HTML
	 *
	 * @param html the HTML representing the PDF
	 * @param pdfFileName the PDF file name to be generated
	 * @return the file representing the generated PDF
	 * @throws PdfConverterException the pdf converter exception
	 */
	File generatePdfFromHtml(String html, String pdfFileName) throws PdfConverterException;

	/**
	 * Generate a PDF from HTML
	 *
	 * @param html the HTML representing the PDF
	 * @param pdfFileName the PDF file name to be generated
	 * @param resourcesBaseUri the base uri to load resources for the PDF
	 *            generation
	 * @return the file representing the generated PDF
	 * @throws PdfConverterException the pdf converter exception
	 */
	File generatePdfFromHtml(String html, String pdfFileName, String resourcesBaseUri) throws PdfConverterException;

	/**
	 * Generate a PDF from map
	 *
	 * @param fieldValueMap the map with the field name and its value
	 * @return the byte[] representing the generated PDF
	 * @throws PdfConverterException the pdf converter exception
	 */
	byte[] generatePdfFromKeyValueMap(Map<String, Object> fieldValueMap) throws PdfConverterException;

	/**
	 * Generate a PDF from map
	 *
	 * @param title the title
	 * @param fieldValueMap the map with the field name and its value
	 * @return the byte[] representing the generated PDF
	 * @throws PdfConverterException the pdf converter exception
	 */
	byte[] generatePdfFromKeyValueMap(String title, Map<String, Object> fieldValueMap) throws PdfConverterException;

	/**
	 * Generate a PDF key-value pairs
	 *
	 * @param title the title
	 * @param keyValuePair a list of key-value pair
	 * @return the byte[] representing the generated PDF
	 * @throws PdfConverterException the pdf converter exception
	 */
	byte[] generatePdfFromKeyValuePairs(String title, List<KeyValuePair> keyValuePair) throws PdfConverterException;

	String readPdfContent(InputStream inputStream) throws IOException;
}
